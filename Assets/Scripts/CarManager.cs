using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;

public enum InputSystemMode
{
    KeyBoard, Gamepad // etc
}

[System.Serializable]
public class MyCar
{
    public float MaxCarSpeed = 7.0f;
    public float MaxCarSteer = 2.0f;
    public float BreakFactor = 0.2f;

    public float CarAcceleration = 0.0f;
    public float Steer = 0.0f;
}
public class CarManager : MonoBehaviour
{
   public MyCar m_Car;

    public InputSystemMode CarControlMode;
    public AudioSource m_CrashSFX;


    bool AccelFwd, AccelBwd;
    bool TouchAccel, TouchBack, TouchBreaks;
    bool SteerLeft, SteerRight;

    public InputActionMap gameplayActions;

    bool m_MoveForward = false;
    bool m_MoveBack = false;
    bool m_MoveLeft = false;
    bool m_MoveRight = false;
    bool m_Brake = false;
    
    void Start()
    {
        m_Car = new MyCar();
        gameplayActions["MoveForward"].performed += MoveForward;
        gameplayActions["StopMoveForward"].performed += StopMoveForward;   
        gameplayActions["MoveBack"].performed += MoveBack;
        gameplayActions["StopMoveBack"].performed += StopMoveBack;
        gameplayActions["MoveLeft"].performed += MoveLeft;
        gameplayActions["StopMoveLeft"].performed += StopMoveLeft;
        gameplayActions["MoveRight"].performed += MoveRight;
        gameplayActions["StopMoveRight"].performed += StopMoveRight;
        gameplayActions["Brake"].performed += Brake;
        gameplayActions["StopBrake"].performed += StopBrake;
        gameplayActions.Enable();
    }


    void FixedUpdate()
    {

        if (CarControlMode == InputSystemMode.KeyBoard)
        {
            if (m_MoveForward)
                Accel(1);             //Accelerate  forward 
            else if (m_MoveBack)
                Accel(-1);         //Accelerate  backward 
            else if (m_Brake)
            {
                if (AccelFwd)
                    StopAccel(1, m_Car.BreakFactor);       //Breaks
                else if (AccelBwd)
                    StopAccel(-1, m_Car.BreakFactor);      //Breaks 
            }
            else
            {
                if (AccelFwd)
                    StopAccel(1, 0.1f);      //Applies breaks slowly 
                else if (AccelBwd)
                    StopAccel(-1, 0.1f);      //Applies breaks slowly 
            }
        }
    }


  

    public void Accel(int Direction)
    {
        if (Direction == 1)
        {
            AccelFwd = true;
            if (m_Car.CarAcceleration <= m_Car.MaxCarSpeed) 
            {
                m_Car.CarAcceleration += 0.05f;
            }

            if (CarControlMode == InputSystemMode.KeyBoard)
            {
                if (m_MoveLeft)
                    transform.Rotate(Vector3.forward * m_Car.Steer);              //Steer left
                if (m_MoveRight)
                    transform.Rotate(Vector3.back * m_Car.Steer);             //steer right
            }

        }
        else if (Direction == -1)
        {
            AccelBwd = true;
            if ((-1 * m_Car.MaxCarSpeed) <= m_Car.CarAcceleration)
            {
                m_Car.CarAcceleration -= 0.05f;
            }

            if (CarControlMode == InputSystemMode.KeyBoard)
            {
                if (m_MoveLeft)
                    transform.Rotate(Vector3.back * m_Car.Steer);             //Steer left in reverse 
                if (m_MoveRight)
                    transform.Rotate(Vector3.forward * m_Car.Steer);      //Steer right  reverse 
            }
           
        }

        if (m_Car.Steer <= m_Car.MaxCarSteer)
            m_Car.Steer += 0.01f;

        if (CarControlMode == InputSystemMode.KeyBoard)
            transform.Translate(Vector2.up * m_Car.CarAcceleration * Time.deltaTime);
    }

    public void StopAccel(int Direction, float BreakingFactor)
    {
        if (Direction == 1)
        {
            if (m_Car.CarAcceleration >= 0.0f)
            {
                m_Car.CarAcceleration -= BreakingFactor;

                if (CarControlMode == InputSystemMode.KeyBoard)
                {
                    if (m_MoveLeft)
                        transform.Rotate(Vector3.forward * m_Car.Steer);
                    if (m_MoveRight)
                        transform.Rotate(Vector3.back * m_Car.Steer);
                }
            }
            else
                AccelFwd = false;
        }
        else if (Direction == -1)
        {
            if (m_Car.CarAcceleration <= 0.0f)
            {
                m_Car.CarAcceleration += BreakingFactor;

                if (CarControlMode == InputSystemMode.KeyBoard)
                {
                    if (m_MoveLeft)
                        transform.Rotate(Vector3.back * m_Car.Steer);
                    if (m_MoveRight)
                        transform.Rotate(Vector3.forward * m_Car.Steer);
                }
                
            }
            else
                AccelBwd = false;
        }

        if (m_Car.Steer >= 0.0f)
            m_Car.Steer -= 0.01f;

        transform.Translate(Vector2.up * m_Car.CarAcceleration * Time.deltaTime);
    }



    public void MoveForward(InputAction.CallbackContext context)
    {
        m_MoveForward = true;
      //  Debug.Log("moving forward");
    }
    public void StopMoveForward(InputAction.CallbackContext context)
    {
        m_MoveForward = false;
       // Debug.Log("Stop moving forward");
    }

    public void MoveBack(InputAction.CallbackContext context)
    {
        m_MoveBack = true;
     //   Debug.Log("moving backward");
    }

    public void StopMoveBack(InputAction.CallbackContext context)
    {
        m_MoveBack = false;
      //  Debug.Log("Stop moving backward");
    }

    public void MoveLeft(InputAction.CallbackContext context)
    {
        m_MoveLeft = true;
       // Debug.Log("moving left");
    }

    public void StopMoveLeft(InputAction.CallbackContext context)
    {
        m_MoveLeft = false;
       // Debug.Log("Stop moving left");
    }

    public void MoveRight(InputAction.CallbackContext context)
    {
        m_MoveRight = true;
       //  Debug.Log("moving Right");
    }

    public void StopMoveRight(InputAction.CallbackContext context)
    {
        m_MoveRight = false;
      //  Debug.Log("Stop moving Right");
    }

    public void Brake(InputAction.CallbackContext context)
    {
        m_Brake = true;
       // Debug.Log("brake ");
    }

    public void StopBrake(InputAction.CallbackContext context)
    {
        m_Brake = false;
        //Debug.Log("Stop brake");
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        m_CrashSFX.Play();
    }
}
